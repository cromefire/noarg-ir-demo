package com.example.demo

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class Controller(val repo: Repo) {
    @GetMapping
    fun test(): Unit {
        val m = Model(string = "test")

        repo.save(m)
    }
}
