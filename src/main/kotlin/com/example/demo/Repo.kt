package com.example.demo

import org.springframework.data.jpa.repository.JpaRepository

interface Repo : JpaRepository<Model, Long>
